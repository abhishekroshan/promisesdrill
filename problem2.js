function promise1() {
  const newPromise1 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("1st Promise");
    }, 3000);
    //created a promise which resolves after 3 seconds
  });
  return newPromise1;
}

function promise2() {
  const newPromise2 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("2nd Promise");
      //created a promise which resolves after 2 seconds
    }, 2000);
  });
  return newPromise2;
}

function promise3() {
  const newPromise3 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("3rd Promise");
      //created a promise which resolves after 4 seconds
    }, 4000);
  });
  return newPromise3;
}

const promisesArray = [promise1(), promise2(), promise3()];
//created array of promises

function composePromises(promises) {
  return Promise.all(promises);
}
//function to compose an array of promises using promise.all

composePromises(promisesArray)
  .then(function (result) {
    console.log(result);
  })
  .catch(function (error) {
    console.log(error);
  });

// Using the composePromise function and handling the result or error
