function racePromise1() {
  const newPromise1 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("Race promise 1 Resolved");
    }, Math.random() * 3000);
    //created a promise which resolves after some time
  });
  return newPromise1;
}

function racePromise2() {
  const newPromise2 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("Race promise 2 Resolved");
    }, Math.random() * 3000);
  });
  return newPromise2;
}

function racePromise() {
  return Promise.race([racePromise1(), racePromise2()]);
  //using promice.race to return the promise which resolves first
}

racePromise()
  .then(function (result) {
    console.log(result);
  })
  .catch(function (error) {
    console.error(error);
  });
// Using the racePromise function and handling the result or error
