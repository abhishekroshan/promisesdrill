// Function to create a promise that resolves after 3 seconds
function promise1() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("1st Promise");
    }, 3000);
    // Created a promise which resolves after 3 seconds
  });
}

// Function to create a promise that resolves after 1 second
function promise2() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("2nd Promise");
    }, 1000);
    // Created a promise which resolves after 1 second
  });
}

// Function to create a promise that resolves after 2.5 seconds
function promise3() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("3rd Promise");
    }, 2500);
    // Created a promise which resolves after 2.5 seconds
  });
}

// Function to create a promise that resolves after 1.5 seconds
function promise4() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("4th Promise");
    }, 1500);
    // Created a promise which resolves after 1.5 seconds
  });
}

// Function to create a promise that resolves after 2 seconds
function promise5() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("5th Promise");
    }, 2000);
    // Created a promise which resolves after 2 seconds
  });
}

function promise6() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("6th Promise");
    }, 2000);
    // Created a promise which resolves after 2 seconds
  });
}

function promise7() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("7th Promise");
    }, 2000);
    // Created a promise which resolves after 2 seconds
  });
}

// Array of promise functions
const promisesArray = [
  promise1,
  promise2,
  promise3,
  promise4,
  promise5,
  promise6,
  promise7,
];

function parallelLimit(promiceArray, limit) {
  //the initial and the limited index which we have to process ata a time
  let initialIndex = 0;
  let finalIndex = limit;

  function helper(array) {
    // Function to execute an array of promises
    return Promise.all(
      // Use Promise.all to execute an array of promise functions
      array.map((element) => {
        return element();
        // Execute each promise function
      })
    );
  }
  const resultArray = [];

  // Recursive function to execute promises within the limit

  function executePromises() {
    const currentPromises = promisesArray.slice(initialIndex, finalIndex);
    // Slice the array to get promises within the current limit

    return helper(currentPromises).then((items) => {
      // Use the helper function to execute promises
      items.forEach((result) => {
        // Adding the results of executed promises to the resultArray
        resultArray.push(result);
      });

      initialIndex = finalIndex;
      // Update the initialIndex for the next recursion call

      if (finalIndex < promiceArray.length) {
        //checking if there are more promises to execute
        finalIndex = finalIndex += limit;
        //if there is we update thr final index value by adding the limit to it
        return executePromises();
        //calling the execute function again
      } else {
        finalIndex = promiceArray.length;
        return resultArray;
        // Return the final resultArray when all promises are executed
      }
    });
  }
  return executePromises();
  //initial function call to start execution
}

parallelLimit(promisesArray, 3)
  .then(function (output) {
    console.log(output);
  })
  .catch(function (error) {
    console.log(error);
  });

//// Using the parellelLimit function and handling the result or error
