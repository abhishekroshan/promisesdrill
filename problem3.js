function promise1() {
  const newPromise1 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("1st Promise");
    }, 3000);
    //created a promise which resolves after 3 seconds
  });
  return newPromise1;
}

function promise2() {
  const newPromise2 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("2nd Promise");
      //created a promise which resolves after 2 seconds
    }, 2000);
  });
  return newPromise2;
}

function promise3() {
  const newPromise3 = new Promise((resolve) => {
    setTimeout(() => {
      resolve("3rd Promise");
      //created a promise which resolves after 4 seconds
    }, 4000);
  });
  return newPromise3;
}

const promisesArray = [promise1(), promise2(), promise3()];
//created array of promises

function dynamicChain(promiceArray) {
  // Function to dynamically chain an array of promises
  let chain = Promise.resolve();

  for (let index = 0; index < promiceArray.length; index++) {
    chain = chain.then(function () {
      // returning the promise function to perform the proper chaining
      return promiceArray[index];
    });
  }
  return chain;
}

dynamicChain(promisesArray)
  .then(function (result) {
    console.log(result);
  })
  .catch(function (error) {
    console.error(error);
  });
// Using the dynamicChain function and handling the result or error
